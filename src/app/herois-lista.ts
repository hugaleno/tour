//Variável constante do tipo array
export const HEROIS = [
    //Criar um heroi em cada linha
    { id: 11, nome: "Foibata" },
    { id: 12, nome: "TacaFogo" },
    { id: 13, nome: "Penatabua" },
    { id: 14, nome: "Ohomei" },
    { id: 15, nome: "Deunatelha" },
    { id: 16, nome: "Fogarel" },
    { id: 17, nome: "Zambeta" },
    { id: 18, nome: "Riscafaca" },
    { id: 19, nome: "BalaPerdida" },
    { id: 20, nome: "Rubinho" }
]