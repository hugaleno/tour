import { Heroi } from './../../app/heroi';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'heroi-detalhe-component',
    templateUrl: 'heroi-detalhe.component.html'
})
export class HeroiDetalheComponent {

    @Input() heroi: Heroi;

    constructor(){}
}