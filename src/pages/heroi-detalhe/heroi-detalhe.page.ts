import { Heroi } from './../../app/heroi';
import { NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    selector:'heroi-detalhe-page',
    templateUrl:'heroi-detalhe.page.html'
})
export class HeroiDetalhePage {

    heroi: Heroi;

    constructor(
        public navParams: NavParams
    ){
        this.heroi = navParams.get("heroiclicado");
        console.log(this.heroi);
    }
}