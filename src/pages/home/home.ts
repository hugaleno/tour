import { Observable } from 'rxjs/Observable';
import { HeroisProvider } from './../../app/herois.provider';
import { Heroi } from './../../app/heroi';
import { Component } from '@angular/core';
import { NavController, LoadingController, Loading } from 'ionic-angular';
import { HeroiDetalhePage } from '../heroi-detalhe/heroi-detalhe.page';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  herois: Heroi[];
  texto: string;
  heroiSelecionado: Heroi;

  constructor(
    public navCtrl: NavController,
    //Importou o provider
    public heroisProvider: HeroisProvider,
    public loadingCtrl: LoadingController
  ) {
   let loading: Loading = loadingCtrl.create({
        content:"Por favor aguarde..."
      });
      loading.present();
      heroisProvider
      .getHeroisTimeout()
      .then((observable: Observable<Heroi[]>) => {
        observable.subscribe( (herois:Heroi[]) => {
          this.herois = herois;
          loading.dismiss();
        });
      });

       // Comentário de linha
    /* Comentário de bloco */
    /* Inicializando os herois 
    com o método getHerois do provider */
    /* heroisProvider
      .getHerois()
      .subscribe( (herois: Heroi[]) => {
        this.herois  = herois;
      } ); */
  }

  selecionarHeroi(heroi: Heroi): void {
    //console.log("Heroi selecionado: ", heroi);
    this.heroiSelecionado = heroi;
    this.navCtrl.push(HeroiDetalhePage, {
      heroiclicado: heroi
    });
  }

  mostraArray(): void {
    console.log("Meu array alterado", this.herois );
  }
}